import React, { Component } from 'react';
import { Page, Grid, Card, List, Header, Table, Tag } from 'tabler-react';
import { getProbabilityRange, getImpactRange } from '../database';
import { getCategoryColorProb, getCategoryColorImpact } from '../helpers/util';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prob_range: [],
      impact_range: []
    };
  }

  componentDidMount() {
    this.getProbabilityRange();
    this.getImpactRange();
  }

  getProbabilityRange = () => {
    getProbabilityRange().then(result => {
      if (result) {
        this.setState({
          prob_range: result
        });
      }
    });
  };

  getImpactRange = () => {
    getImpactRange().then(result => {
      if (result) {
        this.setState({
          impact_range: result
        });
      }
    });
  };

  renderProbTable = () => {
    const { prob_range } = this.state;
    const body =
      prob_range && prob_range.length > 0
        ? prob_range
            .sort((a, b) => a.value > b.value)
            .map(item => this.renderItemProb(item))
        : '';
    return (
      <Table striped style={{ margin: 0, textAlign: 'center' }}>
        <Table.Header>
          <Table.ColHeader>Categoría</Table.ColHeader>
          <Table.ColHeader>Valor</Table.ColHeader>
          <Table.ColHeader>Color</Table.ColHeader>
        </Table.Header>
        <Table.Body>{body}</Table.Body>
      </Table>
    );
  };

  renderItemProb(item) {
    return (
      <Table.Row>
        <Table.Col>{item.name}</Table.Col>
        <Table.Col>{item.value}</Table.Col>
        <Table.Col>
          <Tag color={getCategoryColorProb(item.name.toUpperCase()).name}>
            {item.name}
          </Tag>
        </Table.Col>
      </Table.Row>
    );
  }

  renderImpactTable = () => {
    const { impact_range } = this.state;
    const body =
      impact_range && impact_range.length > 0
        ? impact_range
            .sort((a, b) => a.value > b.value)
            .map(item => this.renderItemImpact(item))
        : '';
    return (
      <Table striped style={{ margin: 0, textAlign: 'center' }}>
        <Table.Header>
          <Table.ColHeader>Categoría</Table.ColHeader>
          <Table.ColHeader>Valor</Table.ColHeader>
          <Table.ColHeader>Color</Table.ColHeader>
        </Table.Header>
        <Table.Body>{body}</Table.Body>
      </Table>
    );
  };

  renderItemImpact(item) {
    return (
      <Table.Row>
        <Table.Col>{item.name}</Table.Col>
        <Table.Col>{item.value}</Table.Col>
        <Table.Col>
          <Tag color={getCategoryColorImpact(item.name.toUpperCase()).name}>
            {item.name}
          </Tag>
        </Table.Col>
      </Table.Row>
    );
  }

  render() {
    var styles = {
      backgroundImage:
        'url(https://firebasestorage.googleapis.com/v0/b/procesos-neflity.appspot.com/o/files%2Fidea.png?alt=media&token=d815a89d-ae35-4c96-a802-ea2bf2893f78)',
      width: '400px'
    };
    return (
      <Page>
        <Page.Header>
          <Header.H1>Procesos de Ingeniería de Software</Header.H1>
        </Page.Header>
        <Page.Main>
          <Grid.Row>
            <Grid.Col width={12}>
              <Card>
                <Card.Header>
                  <Card.Title>Objetivos</Card.Title>
                </Card.Header>
                <List.Group>
                  <List.GroupItem action icon="check-square">
                    Definir, administrar y controlar los riesgos de un proyecto,
                    usando la matriz de probabilidad e impacto.
                  </List.GroupItem>
                  <List.GroupItem action icon="check-square">
                    Definir, administrar y controlar el estado de un proyecto
                    mediante el manejo de la técnica de Earned Value (Valor
                    Ganado)
                  </List.GroupItem>
                </List.Group>
              </Card>
            </Grid.Col>
          </Grid.Row>
          <Grid.Row>
            <Grid.Col width={6}>
              <Card>
                <Card.Header>
                  <Card.Title>Matriz de Impacto</Card.Title>
                </Card.Header>
                {this.renderImpactTable()}
              </Card>
            </Grid.Col>
            <Grid.Col width={6}>
              <Card>
                <Card.Header>
                  <Card.Title>Matriz de Probabilidad</Card.Title>
                </Card.Header>
                {this.renderProbTable()}
              </Card>
            </Grid.Col>
          </Grid.Row>
        </Page.Main>
      </Page>
    );
  }
}
