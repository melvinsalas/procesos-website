import React, { Component } from 'react'
import { Card, Form, Button, Alert, Grid } from 'tabler-react'
import { login, resetPassword } from '../helpers/auth'

function setErrorMsg(error) {
  return {
    loginMessage: error
  }
}

export default class Login extends Component {
  state = { loginMessage: null }
  handleSubmit = (e) => {
    e.preventDefault()
    login(this.email.value, this.pw.value)
      .catch((error) => {
        this.setState(setErrorMsg('Invalid username/password.'))
      })
  }
  resetPassword = () => {
    resetPassword(this.email.value)
      .then(() => this.setState(setErrorMsg(`Password reset email sent to ${this.email.value}.`)))
      .catch((error) => this.setState(setErrorMsg(`Email address not found.`)))
  }
  render() {
    return (
      <Grid.Row>
        <Grid.Col xl={4} offsetXl={4} md={6} offsetMd={3} sm={12}>
          <Card>
            <Card.Body>
              <Card.Title>Login to your Account</Card.Title>
              {
                this.state.loginMessage &&
                <Alert type="danger">
                  {this.state.loginMessage} <a href="#" onClick={this.resetPassword} className="alert-link">Forgot Password?</a>
                </Alert>
              }
              <form onSubmit={this.handleSubmit}>
                <Form.Group label="Email Address">
                  <input className="form-control" ref={(email) => this.email = email} placeholder="Email" />
                </Form.Group>
                <Form.Group label="Password">
                  <input type="password" className="form-control" placeholder="Password" ref={(pw) => this.pw = pw} />
                </Form.Group>
                <Button block type="submit" color="primary">Login</Button>
              </form>
            </Card.Body>
          </Card>
        </Grid.Col>
      </Grid.Row>
    )
  }
}
