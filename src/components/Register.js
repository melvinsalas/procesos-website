import React, { Component } from 'react'
import { Grid, Card, Alert, Form, Button } from 'tabler-react'
import { auth } from '../helpers/auth'

function setErrorMsg(error) {
  return {
    registerError: error.message
  }
}

export default class Register extends Component {
  state = { registerError: null }
  handleSubmit = (e) => {
    e.preventDefault()
    auth(this.email.value, this.pw.value)
      .catch(e => this.setState(setErrorMsg(e)))
  }
  render() {
    return (
      <Grid.Row>
        <Grid.Col xl={4} offsetXl={4} md={6} offsetMd={3} sm={12}>
          <Card>
            <Card.Body>
              <Card.Title>Register</Card.Title>
              {
                this.state.registerError &&
                <Alert type="danger">{this.state.registerError}</Alert>
              }
              <form onSubmit={this.handleSubmit}>
                <Form.Group label="Email Address">
                  <input className="form-control" ref={(email) => this.email = email} placeholder="Email" />
                </Form.Group>
                <Form.Group label="Password">
                  <input type="password" className="form-control" placeholder="Password" ref={(pw) => this.pw = pw} />
                </Form.Group>
                <Button block type="submit" color="primary">Login</Button>
              </form>
            </Card.Body>
          </Card>
        </Grid.Col>
      </Grid.Row>
    )
  }
}
