import React, { Component } from 'react'
import { Card, Button, Dimmer, Form } from 'tabler-react'
import Modal from 'react-modal';
import { getProject } from '../../database';
import { updateProject } from '../../database';

class Detail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      project: null,
      isModalOpen: false,
      name: null,
      description: null,
    };
  }

  componentDidMount() {
    this.getProject(this.props.id);
  }

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  getProject = id => {
    getProject(id).then(result => {
      this.setState({
        project: result,
        name: result.name,
        description: result.description,
      });
    });
  };

  onNameChange = e => {
    this.setState({
      name: e.target.value
    });
  };

  onDescriptionChange = e => {
    this.setState({
      description: e.target.value
    });
  };

  onUpdateProject = e => {
    const { project, name, description } = this.state;
    e.preventDefault();
    updateProject(project.id, name, description).then(() => {
      this.getProject(this.props.id);
      this.closeModal();
    });
  };

  renderModal() {
    const { name, description, isModalOpen } = this.state;
    const modalStyles = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '450px'
      }
    };
    return (
      <Modal isOpen={isModalOpen} onRequestClose={this.closeModal} style={modalStyles}>
        <Form.FieldSet>
          <Form.Group label="Nombre del proyecto" isRequired>
            <Form.Input onChange={this.onNameChange} name="name" value={name} />
          </Form.Group>
          <Form.Group label="Descripción" isRequired>
            <Form.Textarea
              onChange={this.onDescriptionChange}
              name="description"
              rows={6}
              value={description}
            />
          </Form.Group>
        </Form.FieldSet>
        <Button block color="primary" onClick={this.onUpdateProject}>
          Actualizar
        </Button>
      </Modal>
    );
  }

  renderDetail() {
    return this.state.project.description;
  }

  render() {
    return (
      <Card>
        <Card.Header>
          <Card.Title>Detalle de proyecto</Card.Title>
          <Card.Options>
            <Button color="info" icon="edit-2" onClick={this.openModal}>Editar</Button>
          </Card.Options>
        </Card.Header>
        <Card.Body>
          {this.state.project ? this.renderDetail() : <Dimmer active loader />}
        </Card.Body>
        {this.state.project && this.renderModal()}
      </Card>
    )
  }
}

export { Detail }