import React, { Component } from 'react';
import { Card, Table, Tag } from 'tabler-react';
import ReactTooltip from 'react-tooltip';
import { FormattedNumber, IntlProvider } from 'react-intl';

class EarnedValueMatrix extends Component {

  renderTable() {
    const { tasks } = this.props;

    if (!tasks || tasks.length === 0) {
      return this.renderVoid();
    }

    const body = tasks.map(item => this.renderItem(item));

    return (
      <Table striped style={{ margin: 0, textAlign: 'center' }}>
        <Table.Header>
          <Table.ColHeader>Nombre</Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='pv'>PV</a></Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='ev'>EV</a></Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='ac'>AC</a></Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='sv'>SV</a></Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='cv'>CV</a></Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='spi'>SPI</a></Table.ColHeader>
          <Table.ColHeader><a data-tip data-for='cpi'>CPI</a></Table.ColHeader>
        </Table.Header>
        <Table.Body>{body}</Table.Body>
        {this.renderTooltips()}
      </Table>
    );
  }

  renderTooltips() {
    return (
      <div>
        <ReactTooltip id='pv' effect="solid" className="text-center">
          <b>Planned Value</b>
          <p><small>Resource Value x Item Duration</small></p>
        </ReactTooltip>
        <ReactTooltip id='ev' effect="solid" className="text-center">
          <b>Earned Value</b>
          <p><small>Base Cost x Percentage Completed</small></p>
        </ReactTooltip>
        <ReactTooltip id='ac' effect="solid" className="text-center">
          <b>Actual Cost</b>
          <p><small>Worked Hours x Resource Value</small></p>
        </ReactTooltip>
        <ReactTooltip id='sv' effect="solid" className="text-center">
          <b>Schedule Variance</b>
          <p><small>Earned Value - Planned Value</small></p>
        </ReactTooltip>
        <ReactTooltip id='cv' effect="solid" className="text-center">
          <b>Cost Variance</b>
          <p><small>Earned Value - Actual Cost</small></p>
        </ReactTooltip>
        <ReactTooltip id='spi' effect="solid" className="text-center">
          <b>Schedule Performance Index</b>
          <p><small>Earned Value / Planned Value</small></p>
        </ReactTooltip>
        <ReactTooltip id='cpi' effect="solid" className="text-center">
          <b>Cost Performance Index</b>
          <p><small>Earned Value / Actual Cost</small></p>
        </ReactTooltip>
      </div>
    );
  }

  renderItem(item) {
    console.log(this.props);
    const resource = this.props.resources.find(
      x => x.id === item.resourceId
    );
    const pv = resource.value * item.duration;
    const ev = item.baseCost * item.porcentageCompleted / 100;
    const ac = item.workedHours * resource.value;
    return (
      <Table.Row>
        <Table.Col>{item.name}</Table.Col>
        <Table.Col><FormattedNumber value={pv} style="currency" currency="USD" /></Table.Col>
        <Table.Col><FormattedNumber value={ev} style="currency" currency="USD" /></Table.Col>
        <Table.Col><FormattedNumber value={ac} style="currency" currency="USD" /></Table.Col>
        <Table.Col><FormattedNumber value={ev - pv} style="currency" currency="USD" /></Table.Col>
        <Table.Col><FormattedNumber value={ev - ac} style="currency" currency="USD" /></Table.Col>
        <Table.Col><FormattedNumber value={ev / pv} /></Table.Col>
        <Table.Col><FormattedNumber value={ev / ac} /></Table.Col>
      </Table.Row>
    );
  }

  renderVoid() {
    return (
      <Card.Body className="text-center">
        <Tag>Sin registros</Tag>
      </Card.Body>
    );
  }

  render() {
    return (
      <IntlProvider locale="en">
        <Card>
          <Card.Header>
            <Card.Title>Tabla de Valor Ganado</Card.Title>
          </Card.Header>
          {this.renderTable()}
        </Card>
      </IntlProvider>
    );
  }
}

export { EarnedValueMatrix };
