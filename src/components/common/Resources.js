import React, { Component } from 'react';
import { Card, Button } from 'tabler-react';
import { storage } from '../../config/constants';
import FileUploader from 'react-firebase-file-uploader';
import { getProject, uploadFile, getFiles } from '../../database';

class Resources extends Component {
  state = {
    project: null,
    files: [],
    username: '',
    avatar: '',
    isUploading: false,
    progress: 0,
    avatarURL: ''
  };

  componentDidMount() {
    this.getProject(this.props.id);
  }

  getProject = id => {
    getProject(id).then(result => {
      this.setState({
        project: result
      });
      this.getFiles(result.id);
    });
  };

  getFiles = projectId => {
    getFiles(projectId).then(result => {
      this.setState({
        files: result
      });
    });
  };

  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });

  handleProgress = progress => this.setState({ progress });

  handleUploadError = error => {
    this.setState({ isUploading: false });
    console.error(error);
  };

  handleUploadSuccess = filename => {
    this.setState({ avatar: filename, progress: 100, isUploading: false });
    uploadFile(this.state.project.id, filename, () =>
      this.getFiles(this.state.project.id)
    );
  };

  renderList() {
    const { files } = this.state;
    return (
      <ul>
        {files.map(file => (
          <li>{file.url}</li>
        ))}
      </ul>
    );
  }

  render() {
    console.log('files', this.state.files);
    return (
      <Card>
        {this.state.isUploading && <p>Progress: {this.state.progress}</p>}

        {this.state.avatarURL && <img src={this.state.avatarURL} alt="" />}

        <Card.Header>
          <Card.Title>Recursos</Card.Title>
          <Card.Options>
            <Button color="primary" icon="plus">
              Agregar
            </Button>
          </Card.Options>
        </Card.Header>
        <FileUploader
          name="avatar"
          randomizeFilename
          storageRef={storage.ref('files')}
          onUploadStart={this.handleUploadStart}
          onUploadError={this.handleUploadError}
          onUploadSuccess={this.handleUploadSuccess}
          onProgress={this.handleProgress}
        />
        <Card.Body>
          {this.state.files && this.state.files.length > 0 && this.renderList()}
        </Card.Body>
      </Card>
    );
  }
}

export { Resources };
