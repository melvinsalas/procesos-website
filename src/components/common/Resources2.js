import React, { Component } from 'react';
import { Card, Button, Table, Icon, Tag, Dimmer, Form } from 'tabler-react';
import Modal from 'react-modal';
import {
  getResources,
  createResource,
  updateResource,
  deleteResource
} from '../../database';

class Resources2 extends Component {
  state = {
    resources: {},
    loading: true,
    isModalOpen: false,
    modalResource: {
      id: null,
      name: '',
      value: ''
    }
  };

  componentDidMount() {
    this.getResources(this.props.projectId);
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      modalResource: {
        id: null,
        name: '',
        value: ''
      }
    });
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  getResources = id => {
    getResources(id).then(result => {
      this.setState({
        resources: result,
        loading: false
      });
    });
  };

  renderTable() {
    const { resources } = this.state;

    if (!resources || resources.length === 0) {
      return this.renderVoid();
    }

    const body = resources.map(item => this.renderItem(item));

    return (
      <Table striped style={{ margin: 0, textAlign: 'center' }}>
        <Table.Header>
          <Table.ColHeader>Nombre</Table.ColHeader>
          <Table.ColHeader>Precio</Table.ColHeader>
        </Table.Header>
        <Table.Body>{body}</Table.Body>
      </Table>
    );
  }

  renderItem(item) {
    return (
      <Table.Row>
        <Table.Col>{item.name}</Table.Col>
        <Table.Col>
          <Tag>
            ${item.value}
            /h
          </Tag>
        </Table.Col>
        <Table.Col>
          <Icon
            name="edit-3"
            link
            onClick={() => this.onEditResource(item.id)}
          />{' '}
          <Icon
            name="trash"
            link
            onClick={() => this.onDeleteResource(item.id)}
          />
        </Table.Col>
      </Table.Row>
    );
  }

  onDeleteResource(id) {
    deleteResource(id).then(() => {
      this.getResources(this.props.projectId);
      this.props.callback();
    });
  }

  onEditResource(id) {
    this.onIdChange(id);
    this.openModal();
    this.props.callback();
  }

  renderVoid() {
    return (
      <Card.Body className="text-center">
        <Tag>Sin registros</Tag>
      </Card.Body>
    );
  }

  renderLoading() {
    return (
      <Card.Body>
        <Dimmer active loader />
      </Card.Body>
    );
  }

  renderModal() {
    const modalStyles = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '450px'
      }
    };

    const buttons =
      this.state.modalResource.id == null ? (
        <Button block color="primary" onClick={this.onCreateResource}>
          Agregar
        </Button>
      ) : (
        <Button block color="primary" onClick={this.onUpdateResource}>
          Actualizar
        </Button>
      );

    const { modalResource } = this.state;

    return (
      <Modal
        isOpen={this.state.isModalOpen}
        onRequestClose={this.closeModal}
        style={modalStyles}
      >
        <Form.FieldSet>
          <Form.Group label="Nombre del recurso" isRequired>
            <Form.Input
              onChange={this.onNameChange}
              value={modalResource.name}
            />
          </Form.Group>
          <Form.Group label="Precio por hora" isRequired>
            <Form.Input
              onChange={this.onValueChange}
              value={modalResource.value}
            />
          </Form.Group>
        </Form.FieldSet>
        {buttons}
      </Modal>
    );
  }

  onIdChange = id => {
    const { resources } = this.state;
    const modalResource = resources.find(x => x.id === id);
    this.setState({
      modalResource: JSON.parse(JSON.stringify(modalResource))
    });
  };

  onNameChange = e => {
    const { modalResource } = this.state;
    modalResource.name = e.target.value;
    this.setState({ modalResource });
  };

  onValueChange = e => {
    const { modalResource } = this.state;
    modalResource.value = parseFloat(e.target.value);
    this.setState({ modalResource });
  };

  onCreateResource = () => {
    const { name, value } = this.state.modalResource;
    const { projectId } = this.props;
    createResource(projectId, name, value).then(() => {
      this.closeModal();
      this.getResources(projectId);
      this.props.callback();
    });
  };

  onUpdateResource = () => {
    updateResource(this.state.modalResource).then(() => {
      this.getResources(this.props.projectId);
      this.closeModal();
      this.props.callback();
    });
  };

  render() {
    const { loading } = this.state;
    const body = loading ? this.renderLoading() : this.renderTable();
    return (
      <Card>
        <Card.Header>
          <Card.Title>Recursos</Card.Title>
          <Card.Options>
            <Button
              color="success"
              outline
              pill
              icon="plus"
              size="sm"
              onClick={this.openModal}
            >
              Agregar
            </Button>
          </Card.Options>
        </Card.Header>
        {body}
        {this.renderModal()}
      </Card>
    );
  }
}

export { Resources2 };
