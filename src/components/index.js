import React, { Component } from 'react';
import 'tabler-react/dist/Tabler.css';
import { Route, BrowserRouter, Redirect, Switch } from 'react-router-dom';
import { Page, Nav, Grid } from 'tabler-react';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import Dashboard from './protected/Dashboard';
import Project from './protected/Project';
import Project2 from './protected/Project2';
import { logout } from '../helpers/auth';
import { firebaseAuth } from '../config/constants';

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        authed === true ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        )
      }
    />
  );
}

function PublicRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        authed === false ? (
          <Component {...props} />
        ) : (
          <Redirect to="/dashboard" />
        )
      }
    />
  );
}

export default class App extends Component {
  state = {
    authed: false,
    loading: true,
    email: null
  };
  componentDidMount() {
    this.removeListener = firebaseAuth().onAuthStateChanged(user => {
      if (user) {
        this.setState({
          authed: true,
          loading: false,
          email: user.email
        });
      } else {
        this.setState({
          authed: false,
          loading: false,
          email: null
        });
      }
    });
  }
  componentWillUnmount() {
    this.removeListener();
  }
  render() {
    return this.state.loading === true ? (
      <h1>Loading</h1>
    ) : (
      <BrowserRouter>
        <Page.Main>
          <div className="header d-lg-flex p-0 collapse">
            <div className="container">
              <Grid.Row>
                {this.state.authed ? (
                  <Nav className="border-0">
                    <Nav.Item to="/" icon="home">
                      Home
                    </Nav.Item>
                    <Nav.Item to="/dashboard" icon="grid">
                      Proyectos
                    </Nav.Item>
                    <Nav.Item
                      icon="log-out"
                      onClick={() => {
                        logout();
                      }}
                    >
                      Logout
                    </Nav.Item>
                    <Nav.Item icon="user">{this.state.email}</Nav.Item>
                  </Nav>
                ) : (
                  <Nav className="border-0">
                    <Nav.Item to="/" icon="home">
                      Home
                    </Nav.Item>
                    <Nav.Item to="/login" icon="log-in">
                      Login
                    </Nav.Item>
                    <Nav.Item to="/register" icon="user">
                      Register
                    </Nav.Item>
                  </Nav>
                )}
              </Grid.Row>
            </div>
          </div>
          <Page.Content>
            <Switch>
              <Route path="/" exact component={Home} />
              <PublicRoute
                authed={this.state.authed}
                path="/login"
                component={Login}
              />
              <PublicRoute
                authed={this.state.authed}
                path="/register"
                component={Register}
              />
              <PrivateRoute
                authed={this.state.authed}
                path="/dashboard"
                component={Dashboard}
              />
              <PrivateRoute
                authed={this.state.authed}
                path="/project2/:id"
                component={Project2}
              />
              <PrivateRoute
                authed={this.state.authed}
                path="/project/:id"
                component={Project}
              />
              <Route render={() => <h3>No Match</h3>} />
            </Switch>
          </Page.Content>
        </Page.Main>
      </BrowserRouter>
    );
  }
}
