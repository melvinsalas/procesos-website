import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Modal from 'react-modal';
import {
  Page,
  Card,
  Grid,
  Button,
  Dimmer,
  Form,
  Alert,
  Icon,
  Tag
} from 'tabler-react';
import {
  getUserProjects,
  createProject,
  deleteProject,
  updateProject
} from '../../database';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
      loading: true,
      isModalOpen: false,
      alert: null,
      modalProject: {
        id: '',
        name: '',
        description: ''
      }
    };
  }

  componentDidMount() {
    this.loadProjects();
  }

  loadProjects = () => {
    getUserProjects().then(result => {
      this.setState({
        projects: result,
        loading: false
      });
    });
  };

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      modalProject: {
        id: '',
        name: '',
        description: ''
      }
    });
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  onNameChange = e => {
    this.setState({
      modalProject: {
        ...this.state.modalProject,
        name: e.target.value
      }
    });
  };

  onDescriptionChange = e => {
    this.setState({
      modalProject: {
        ...this.state.modalProject,
        description: e.target.value
      }
    });
  };

  onCreateProject = e => {
    e.preventDefault();
    createProject(
      this.state.modalProject.name,
      this.state.modalProject.description
    ).then(result => {
      this.loadProjects();
      this.closeModal();
      this.onShowAlert('El proyecto se ha creado con éxito!', 'success');
    });
  };

  onDeleteProject = projectId => {
    console.log(`Delete project: ${projectId}`);
    deleteProject(projectId).then(result => {
      this.loadProjects();
      result
        ? this.onShowAlert('El proyecto fue borrado', 'danger')
        : this.onShowAlert('El proyecto NO fue borrado', 'danger');
    });
  };

  onEditProyecto = id => {
    const project = this.state.projects.find(item => item.id === id);
    this.setState({
      modalProject: {
        id,
        name: project.name,
        description: project.description
      }
    });
    this.openModal();
  };

  updateProject = () => {
    updateProject(this.state.modalProject).then(() => {
      this.closeModal();
      this.loadProjects();
    });
  };

  onShowAlert = (text, type) => {
    this.setState({
      alert: { text, type }
    });
    setTimeout(() => {
      this.setState({
        alert: null
      });
    }, 3000);
  };

  renderVoid() {
    return (
      <Card>
        <Card.Header>
          <Card.Title>No tiene Proyectos Iniciados</Card.Title>
        </Card.Header>
        <Card.Body className="text-center">
          <Tag>Puede crear un nuevo proyecto en el botón "Agregar"</Tag>
        </Card.Body>
        {this.renderModal()}
      </Card>
    );
  }

  renderList() {
    const { projects } = this.state;

    if (!projects || projects.length === 0) {
      return this.renderVoid();
    }

    return (
      <Page.Main>
        <Grid.Row>
          {projects.map(item => (
            <Grid.Col width={4} key={item.id}>
              <Card key={item.id}>
                <Card.Header>
                  <Card.Title>{item.name}</Card.Title>
                  <Card.Options>
                    <Icon
                      name="edit-3"
                      link
                      onClick={() => this.onEditProyecto(item.id)}
                    />
                    <Button
                      size="sm"
                      icon="x"
                      onClick={() => this.onDeleteProject(item.id)}
                    />
                  </Card.Options>
                </Card.Header>
                <Card.Body>{item.description}</Card.Body>
                <Card.Footer>
                  <NavLink to={{ pathname: `/project2/${item.id}` }}>
                    <Button block color="primary">
                      Ver
                    </Button>
                  </NavLink>
                </Card.Footer>
              </Card>
            </Grid.Col>
          ))}
        </Grid.Row>
        {this.renderModal()}
      </Page.Main>
    );
  }

  renderModal() {
    const modalStyles = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '450px'
      }
    };
    const buttons =
      this.state.modalProject.id === '' ? (
        <Button block color="primary" onClick={this.onCreateProject}>
          Agregar
        </Button>
      ) : (
          <Button block color="primary" onClick={this.updateProject}>
            Actualizar
        </Button>
        );
    return (
      <Modal
        isOpen={this.state.isModalOpen}
        onRequestClose={this.closeModal}
        style={modalStyles}
      >
        <Form.FieldSet>
          <Form.Group label="Nombre del proyecto" isRequired>
            <Form.Input
              onChange={this.onNameChange}
              name="name"
              value={this.state.modalProject.name}
            />
          </Form.Group>
          <Form.Group label="Descripción" isRequired>
            <Form.Textarea
              value={this.state.modalProject.description}
              onChange={this.onDescriptionChange}
              name="description"
              rows={6}
            />
          </Form.Group>
        </Form.FieldSet>
        {buttons}
      </Modal>
    );
  }

  render() {
    const { loading, alert } = this.state;

    const alertHeader = !alert ? null : (
      <Alert type={alert.type} icon="check">
        {alert.text}
      </Alert>
    );

    const options = (
      <Button color="primary" icon="plus" onClick={this.openModal}>
        Agregar
      </Button>
    );

    return (
      <Page>
        {alertHeader}
        <Page.Header title="Proyectos" options={options} />
        {loading ? <Dimmer active loader /> : this.renderList()}
      </Page>
    );
  }
}
