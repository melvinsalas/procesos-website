import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Page, Card, Grid, Button, Alert } from 'tabler-react';
import { Resources, Detail } from '../common';
import { getDataForMatrix } from '../../helpers/util';
import C3Chart from 'react-c3js';
import {
  getProject,
  createMatrix,
  getProjectMatrix,
  getMatrixRisks
} from '../../database';

export default class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project: {},
      alert: null,
      matrixId: null,
      risks: []
    };
  }

  componentDidMount() {
    this.getProject(this.props.match.params.id);
    this.getMatrix(this.props.match.params.id);
  }

  onShowAlert = (text, type) => {
    this.setState({
      alert: { text, type }
    });
    setTimeout(() => {
      this.setState({
        alert: null
      });
    }, 3000);
  };

  getProject = id => {
    getProject(id).then(result => {
      this.setState({
        project: result
      });
    });
  };

  getMatrix = id => {
    getProjectMatrix(id).then(result => {
      if (result.length > 0) {
        this.setState({
          matrixId: result[0].id
        });
        this.loadRisks(result[0].id);
      }
    });
  };

  loadRisks = id => {
    getMatrixRisks(id).then(result => {
      if (result) {
        this.setState({
          risks: result
        });
      }
    });
  };

  onCreateMatrix = () => {
    createMatrix(this.state.project.id).then(result => {
      if (result) {
        this.setState({
          matrixId: result.id
        });
        this.onShowAlert(
          'La matriz fue creada con éxito, has click en editar para comenzar a agregar riesgos',
          'success'
        );
      } else {
        this.onShowAlert('Hubo un error creando la matriz', 'danger');
      }
    });
  };

  render() {
    const { alert, risks } = this.state;

    const alertHeader = !alert ? null : (
      <Alert type={alert.type} icon="check">
        {alert.text}
      </Alert>
    );

    let formattedData;
    if (risks.length > 0) {
      formattedData = getDataForMatrix(risks);
    }

    const data = {
      ...formattedData,
      type: 'donut'
    };

    return (
      <Page>
        {alertHeader}
        <Page.Header title={this.state.project.name} subTitle={'Proyecto'} />
        <Grid.Row>
          <Grid.Col width={6}>
            <Detail id={this.props.match.params.id} />
          </Grid.Col>
          <Grid.Col width={6}>
            <Card>
              <Card.Header>
                <Card.Title>Matriz de riesgos</Card.Title>
                {this.state.matrixId ? (
                  <Card.Options>
                    <NavLink
                      to={{
                        pathname: `${this.props.match.url}/matrix/${
                          this.state.matrixId
                        }`
                      }}
                    >
                      <Button color="info">Ver</Button>
                    </NavLink>
                  </Card.Options>
                ) : (
                  <Card.Options>
                    <Button
                      color="success"
                      onClick={() => this.onCreateMatrix()}
                    >
                      Crear
                    </Button>
                  </Card.Options>
                )}
              </Card.Header>
              <Card.Body>
                {this.state.matrixId && risks.length > 0 ? (
                  <C3Chart data={data} />
                ) : (
                  'No has creado una matriz'
                )}
              </Card.Body>
            </Card>
          </Grid.Col>
          <Grid.Col width={12}>
            <Resources id={this.props.match.params.id} />
          </Grid.Col>
        </Grid.Row>
      </Page>
    );
  }
}
