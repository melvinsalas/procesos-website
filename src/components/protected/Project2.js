import React, { Component } from 'react';
import { Page, Grid } from 'tabler-react';
import { getProject, getResources, getTasks } from '../../database';
import { Resources2, EarnedValueMatrix } from '../common';
import MatrixContainer from './matrix';
import TasksContainer from './tasks';

export default class Project2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project: {},
      resources: null,
      task: null
    };
  }

  componentDidMount() {
    const projectId = this.props.match.params.id;
    this.getProject(projectId);
    this.getResources(projectId);
    this.getTasks(projectId);
  }

  getTasks = id => {
    getTasks(id).then(result => {
      this.setState({
        tasks: result
      });
    });
  };

  getProject = id => {
    getProject(id).then(result => {
      this.setState({
        project: result
      });
    });
  };

  getResources = id => {
    getResources(id).then(result => {
      this.setState({
        resources: result
      });
    });
  };

  render() {
    const { resources, tasks } = this.state;
    return (
      <Page>
        <Page.Header
          title={this.state.project.name}
          subTitle={this.state.project.description}
        />
        <Grid.Row>
          <Grid.Col width={4}>
            <Resources2
              projectId={this.props.match.params.id}
              callback={() => this.getResources(this.props.match.params.id)}
            />
          </Grid.Col>
          <Grid.Col width={8}>
            <TasksContainer
              projectId={this.props.match.params.id}
              resources={resources}
              tasks={tasks}
              callback={() => this.getTasks(this.props.match.params.id)}
            />
          </Grid.Col>
          <Grid.Col width={12}>
            <EarnedValueMatrix
              projectId={this.props.match.params.id}
              resources={resources}
              tasks={tasks}
            />
          </Grid.Col>
          <Grid.Col width={12}>
            <MatrixContainer projectId={this.props.match.params.id} />
          </Grid.Col>
        </Grid.Row>
      </Page>
    );
  }
}
