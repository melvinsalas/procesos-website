import React, { Component } from 'react';
import { Card, Tag } from 'tabler-react';
import { getDataForMatrix } from '../../../helpers/util';
import C3Chart from 'react-c3js';

export default class Graphic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formattedData: {}
    };
  }

  componentDidMount() {
    if (this.props.risks.length > 0) {
      this.updateData();
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.risks &&
      prevProps.risks.length !== this.props.risks.length
    ) {
      this.updateData();
    }
  }

  updateData = () => {
    const formatted = getDataForMatrix(this.props.risks);
    const data = {
      ...formatted,
      type: 'donut'
    };
    this.setState({
      formattedData: data
    });
  };

  render() {
    const { risks, matrixId } = this.props;
    const { formattedData } = this.state;

    return (
      <Card>
        <Card.Header>
          <Card.Title>Gráfico de la matriz</Card.Title>
        </Card.Header>
        <Card.Body className="text-center">
          {formattedData.columns && matrixId && risks.length > 0 ? (
            <C3Chart data={formattedData} unloadBeforeLoad />
          ) : (
              <Tag>No hay riesgos en la matriz</Tag>
            )}
        </Card.Body>
      </Card>
    );
  }
}
