import React, { Component } from 'react';
import { Card, Tag, Button, Form, Table, Dimmer, Icon } from 'tabler-react';
import { getRiskCategory, getCategoryColor } from '../../../helpers/util';
import Modal from 'react-modal';
import { getProbabilityRange, getImpactRange } from '../../../database';

export default class Matrix extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      prob_range: [],
      impact_range: [],
      isModalOpen: false,
      newRiskDescription: null,
      newRiskProbValue: 1,
      newRiskImpactValue: 1
    };
  }

  componentDidMount() {
    this.getProbabilityRange();
    this.getImpactRange();
  }

  getProbabilityRange = () => {
    getProbabilityRange().then(result => {
      if (result) {
        this.setState({
          prob_range: result,
          loading: false,
        });
      }
    });
  };

  getImpactRange = () => {
    getImpactRange().then(result => {
      if (result) {
        this.setState({
          impact_range: result,
          loading: false,
        });
      }
    });
  };

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  onProbChange = e => {
    this.setState({
      newRiskProbValue: this.state.prob_range.find(
        item => item.name === e.target.value
      ).value,
    });
  };

  onImpactChange = e => {
    this.setState({
      newRiskImpactValue: this.state.impact_range.find(
        item => item.name === e.target.value
      ).value
    });
  };

  onDescriptionChange = e => {
    this.setState({
      newRiskDescription: e.target.value
    });
  };

  onDeleteRisk = id => {
    this.props.onDeleteRisk(id);
  };

  onDeleteAllRisks = () => {
    this.props.risks.forEach(risk => {
      this.props.onDeleteRisk(risk.id);
    });
  };

  onCreateRisk = () => {
    const {
      newRiskDescription,
      newRiskImpactValue,
      newRiskProbValue
    } = this.state;
    const riskScore = newRiskImpactValue * newRiskProbValue;
    const category = getRiskCategory(riskScore);
    this.props.onCreateRisk(
      newRiskDescription,
      newRiskImpactValue,
      newRiskProbValue,
      riskScore,
      category
    );
    this.closeModal();
    this.resetOptionState();
  };

  resetOptionState = () => {
    this.setState({
      newRiskDescription: null,
      newRiskProbValue: 1,
      newRiskImpactValue: 1
    });
  };

  render() {
    const { risks } = this.props;

    const body = this.state.loading ? this.renderLoading() : this.renderTable();

    const options = (
      <Button.List align="center">
        <Button color="success" icon="plus" size="sm" outline pill onClick={this.openModal}>Agregar</Button>
        {risks && risks.length > 0 ? (
          <Button color="danger" icon="trash" size="sm" outline pill onClick={this.onDeleteAllRisks}>Borrar todo</Button>
        ) : null}
      </Button.List>
    );

    return (
      <Card>
        <Card.Header>
          <Card.Title>Matriz de probabilidad e impacto por riesgo</Card.Title>
          <Card.Options>{options}</Card.Options>
        </Card.Header>
        {body}
        {this.renderModal()}
      </Card>
    );
  }

  renderLoading() {
    return (
      <Card.Body>
        <Dimmer active loader />
      </Card.Body>
    );
  }

  renderModal() {
    const modalStyles = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '450px'
      }
    };
    const { prob_range, impact_range } = this.state;
    return (
      <Modal
        isOpen={this.state.isModalOpen}
        onRequestClose={this.closeModal}
        style={modalStyles}
      >
        <Form.FieldSet>
          <Form.Group label="Descripción del riesgo" isRequired>
            <Form.Input name="risk" onChange={this.onDescriptionChange} />
          </Form.Group>
          <Form.Group label="Valor de Probabilidad">
            <Form.Select onChange={this.onProbChange}>
              {prob_range.sort((a, b) => a.value > b.value).map(item => (
                <option key={item.id} id={item.value}>
                  {item.name}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
          <Form.Group label="Valor de Impacto">
            <Form.Select onChange={this.onImpactChange}>
              {impact_range.sort((a, b) => a.value > b.value).map(item => (
                <option key={item.id} id={item.value}>
                  {item.name}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
        </Form.FieldSet>
        <Button block color="primary" onClick={this.onCreateRisk}>
          Agregar
        </Button>
      </Modal>
    );
  }

  renderTag(category) {
    const categoryColor = getCategoryColor(category);
    return <Tag color={categoryColor.name}>{category}</Tag>;
  }

  renderTable() {
    const { risks } = this.props;

    if (!risks || risks.length === 0) {
      return this.renderVoid();
    }

    const body = risks.map(item => this.renderItem(item));

    return (
      <Table striped style={{ margin: 0, textAlign: 'center' }}>
        <Table.Header>
          <Table.ColHeader>Riesgo</Table.ColHeader>
          <Table.ColHeader>Probabilidad</Table.ColHeader>
          <Table.ColHeader>Impacto</Table.ColHeader>
          <Table.ColHeader>Nota de riesgo</Table.ColHeader>
          <Table.ColHeader>Categoría</Table.ColHeader>
          <Table.ColHeader />
        </Table.Header>
        <Table.Body>{body}</Table.Body>
      </Table>
    );
  }

  renderVoid() {
    return (
      <Card.Body className="text-center">
        <Tag>Sin registros</Tag>
      </Card.Body>
    );
  }

  renderItem(item) {
    return (
      <Table.Row key={item.id}>
        <Table.Col>{item.description}</Table.Col>
        <Table.Col style={{align: 'center'}}><Tag>{item.probability}</Tag></Table.Col>
        <Table.Col><Tag>{item.impact}</Tag></Table.Col>
        <Table.Col><Tag>{item.score}</Tag></Table.Col>
        <Table.Col>{this.renderTag(item.category)}</Table.Col>
        <Table.Col><Icon name="trash" link onClick={() => this.onDeleteRisk(item.id)} /></Table.Col>
      </Table.Row>
    );
  }
}
