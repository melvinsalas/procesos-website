import React, { Component } from 'react';
import { Grid } from 'tabler-react';
import Graphic from './Graphic';
import Matrix from './Matrix';
import {
  createMatrix,
  getProjectMatrix,
  getMatrixRisks,
  createRisk,
  deleteRisk
} from '../../../database';

export default class MatrixContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null,
      matrixId: null,
      risks: []
    };
  }

  componentDidMount() {
    this.getMatrix(this.props.projectId);
  }

  getMatrix = id => {
    getProjectMatrix(id).then(result => {
      if (result.length > 0) {
        this.setState({
          matrixId: result[0].id
        });
        this.loadRisks(result[0].id);
      } else {
        this.onCreateMatrix();
      }
    });
  };

  loadRisks = id => {
    getMatrixRisks(id).then(result => {
      if (result) {
        this.setState({
          risks: result
        });
      }
    });
  };

  onCreateMatrix = () => {
    createMatrix(this.props.projectId).then(result => {
      if (result) {
        this.setState({
          matrixId: result.id
        });
        this.onShowAlert(
          'La matriz fue creada con éxito, has click en editar para comenzar a agregar riesgos',
          'success'
        );
        this.loadRisks(result.id);
      } else {
        this.onShowAlert('Hubo un error creando la matriz', 'danger');
      }
    });
  };

  onCreateRisk = (
    newRiskDescription,
    newRiskImpactValue,
    newRiskProbValue,
    riskScore,
    category
  ) => {
    createRisk(
      this.state.matrixId,
      newRiskDescription,
      newRiskProbValue,
      newRiskImpactValue,
      riskScore,
      category
    ).then(result => {
      this.loadRisks(this.state.matrixId);
    });
  };

  onDeleteRisk = id => {
    deleteRisk(id).then(result => {
      this.loadRisks(this.state.matrixId);
    });
  };

  onShowAlert = (text, type) => {
    this.setState({
      alert: { text, type }
    });
    setTimeout(() => {
      this.setState({
        alert: null
      });
    }, 3000);
  };

  render() {
    const { projectId } = this.props;
    const { matrixId, risks } = this.state;
    return (
      <Grid.Row>
        <Grid.Col width={4}>
          <Graphic projectId={projectId} matrixId={matrixId} risks={risks} />
        </Grid.Col>
        <Grid.Col width={8}>
          <Matrix
            projectId={projectId}
            matrixId={matrixId}
            risks={risks}
            onCreateRisk={this.onCreateRisk}
            onDeleteRisk={this.onDeleteRisk}
            onDeleteAllRisks={this.onDeleteAllRisks}
          />
        </Grid.Col>
      </Grid.Row>
    );
  }
}
