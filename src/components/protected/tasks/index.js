import React, { Component } from 'react';
import Modal from 'react-modal';
import { Card, Button, Table, Icon, Tag, Dimmer, Form } from 'tabler-react';
import { createTask, updateTask, deleteTask } from '../../../database';

export default class TasksContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isModalOpen: false,
      modalTask: {
        id: null,
        name: '',
        duration: '',
        porcentageCompleted: '',
        resourceId: '',
        workedHours: '',
        baseCost: ''
      }
    };
  }

  componentDidMount() {
    if (this.props.resources && this.props.resources.length > 0) {
      this.setState({
        modalTask: {
          ...this.state.modalTask,
          resourceId: this.props.resources[0].id
        }
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.resources &&
      this.props.resources.length > 0 &&
      JSON.stringify(prevProps.resources) !==
        JSON.stringify(this.props.resources)
    ) {
      this.setState({
        modalTask: {
          ...this.state.modalTask,
          resourceId: this.props.resources[0].id
        }
      });
    }
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false,
      modalTask: {
        id: null,
        name: '',
        duration: '',
        porcentageCompleted: '',
        resourceId: this.props.resources[0].id,
        workedHours: '',
        baseCost: ''
      }
    });
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  renderTable() {
    const { tasks } = this.props;

    if (!tasks || tasks.length === 0) {
      return this.renderVoid();
    }

    const body = tasks.map(item => this.renderItem(item));
    return (
      <Table striped style={{ margin: 0, textAlign: 'center' }}>
        <Table.Header>
          <Table.ColHeader>Nombre</Table.ColHeader>
          <Table.ColHeader>Duración</Table.ColHeader>
          <Table.ColHeader>Completado</Table.ColHeader>
          <Table.ColHeader>Recurso</Table.ColHeader>
          <Table.ColHeader>Horas trabajadas</Table.ColHeader>
          <Table.ColHeader>Base</Table.ColHeader>
          <Table.ColHeader />
        </Table.Header>
        <Table.Body>{body}</Table.Body>
      </Table>
    );
  }

  renderItem(item) {
    const resource = this.props.resources.find(x => x.id === item.resourceId);
    return (
      <Table.Row key={item.id}>
        <Table.Col>{item.name}</Table.Col>
        <Table.Col>
          <Tag>
            {item.duration}
            /h
          </Tag>
        </Table.Col>
        <Table.Col>{item.porcentageCompleted}%</Table.Col>
        <Table.Col>{resource.name}</Table.Col>
        <Table.Col>
          <Tag>
            {item.workedHours}
            /h
          </Tag>
        </Table.Col>
        <Table.Col>
          <Tag>${item.baseCost}</Tag>
        </Table.Col>
        <Table.Col>
          <Icon name="edit-3" link onClick={() => this.onEditTasks(item.id)} />{' '}
          <Icon name="trash" link onClick={() => this.onDeleteTask(item.id)} />
        </Table.Col>
      </Table.Row>
    );
  }

  onDeleteTask(id) {
    deleteTask(id).then(() => {
      this.props.callback();
    });
  }

  onEditTasks(id) {
    this.onIdChange(id);
    this.openModal();
  }

  renderVoid() {
    return (
      <Card.Body className="text-center">
        <Tag>Sin registros</Tag>
      </Card.Body>
    );
  }

  renderLoading() {
    return (
      <Card.Body>
        <Dimmer active loader />
      </Card.Body>
    );
  }

  renderModal() {
    const modalStyles = {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '450px'
      }
    };

    const buttons =
      this.state.modalTask.id == null ? (
        <Button block color="primary" onClick={this.onCreateTask}>
          Agregar
        </Button>
      ) : (
        <Button block color="primary" onClick={this.onUpdateTask}>
          Actualizar
        </Button>
      );

    const { modalTask } = this.state;

    return (
      <Modal
        isOpen={this.state.isModalOpen}
        onRequestClose={this.closeModal}
        style={modalStyles}
      >
        <Form.FieldSet>
          <Form.Group label="Nombre de la tarea" isRequired>
            <Form.Input onChange={this.onNameChange} value={modalTask.name} />
          </Form.Group>
          <Form.Group label="Duración en horas" isRequired>
            <Form.Input
              onChange={this.onDurationChange}
              value={modalTask.duration}
            />
          </Form.Group>
          <Form.Group label="Porcentaje completado" isRequired>
            <Form.Input
              onChange={this.onPorcentageChange}
              value={modalTask.porcentageCompleted}
            />
          </Form.Group>
          <Form.Group label="Recurso asignado">
            <Form.Select
              onChange={this.onResourcesChange}
              value={modalTask.resourceId}
            >
              {this.props.resources &&
                this.props.resources.length > 0 &&
                this.props.resources.map(item => (
                  <option
                    key={item.id}
                    id={item.id}
                    value={item.id}
                  >
                    {item.name}
                  </option>
                ))}
            </Form.Select>
          </Form.Group>
          <Form.Group label="Horas trabajadas" isRequired>
            <Form.Input
              onChange={this.onWorkedHoursChange}
              value={modalTask.workedHours}
            />
          </Form.Group>
          <Form.Group label="Costo Base" isRequired>
            <Form.Input
              onChange={this.onBaseCostChange}
              value={modalTask.baseCost}
            />
          </Form.Group>
        </Form.FieldSet>
        {buttons}
      </Modal>
    );
  }

  onIdChange = id => {
    const { tasks } = this.props;
    const modalTask = tasks.find(x => x.id === id);
    this.setState({
      modalTask: JSON.parse(JSON.stringify(modalTask))
    });
  };

  onNameChange = e => {
    const { modalTask } = this.state;
    modalTask.name = e.target.value;
    this.setState({ modalTask });
  };

  onDurationChange = e => {
    const { modalTask } = this.state;
    modalTask.duration = parseFloat(e.target.value);
    this.setState({ modalTask });
  };

  onResourcesChange = e => {
    const { modalTask } = this.state;
    modalTask.resourceId = this.props.resources.find(
      item => item.name === e.target.value
    ).id;
    this.setState({ modalTask });
  };

  onPorcentageChange = e => {
    const { modalTask } = this.state;
    modalTask.porcentageCompleted = parseFloat(e.target.value);
    this.setState({ modalTask });
  };

  onWorkedHoursChange = e => {
    const { modalTask } = this.state;
    modalTask.workedHours = parseFloat(e.target.value);
    this.setState({ modalTask });
  };

  onBaseCostChange = e => {
    const { modalTask } = this.state;
    modalTask.baseCost = parseFloat(e.target.value);
    this.setState({ modalTask });
  };

  onCreateTask = () => {
    const {
      name,
      duration,
      porcentageCompleted,
      resourceId,
      workedHours,
      baseCost
    } = this.state.modalTask;
    const { projectId } = this.props;
    const resource = this.props.resources.find(x => x.id === resourceId);
    const actualCost = workedHours * resource.value;
    createTask(
      projectId,
      name,
      duration,
      porcentageCompleted,
      resourceId,
      workedHours,
      baseCost,
      actualCost
    ).then(() => {
      this.closeModal();
      this.props.callback();
    });
  };

  onUpdateTask = () => {
    const { workedHours, resourceId } = this.state.modalTask;
    const task = this.state.modalTask;
    const resource = this.props.resources.find(x => x.id === resourceId);
    task.actualCost = workedHours * resource.value;
    updateTask(task).then(() => {
      this.props.callback();
      this.closeModal();
    });
  };

  render() {
    const { loading } = this.state;
    const body = loading ? this.renderLoading() : this.renderTable();
    return (
      <Card>
        <Card.Header>
          <Card.Title>Tareas</Card.Title>
          <Card.Options>
            {this.props.resources && this.props.resources.length > 0 ? (
              <Button
                color="success"
                icon="plus"
                pill
                outline
                size="sm"
                onClick={this.openModal}
              >
                Agregar
              </Button>
            ) : (
              <Tag>Deben haber recursos para agregar tareas</Tag>
            )}
          </Card.Options>
        </Card.Header>
        {body}
        {this.renderModal()}
      </Card>
    );
  }
}
