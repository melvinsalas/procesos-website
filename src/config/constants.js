import firebase from 'firebase';
require('firebase/firestore');

const config = {
  apiKey: 'AIzaSyCIptPuEtoIC9ZA6oABN-U3XXL8D-HRgJ8',
  authDomain: 'procesos-neflity.firebaseapp.com',
  databaseURL: 'https://procesos-neflity.firebaseio.com',
  projectId: 'procesos-neflity',
  storageBucket: "gs://procesos-neflity.appspot.com",
};

firebase.initializeApp(config);

export const ref = firebase.database().ref();
export const firebaseAuth = firebase.auth;
export const db = firebase.firestore();
export const storage = firebase.storage();
