import { storage } from '../config/constants';
import { db } from '../config/constants';

const collection = 'files';
const folder = 'files';

export const getFiles = (projectId) => {
  console.log('projectId', projectId);
  return db
    .collection(collection)
    .where('projectId', '==', projectId)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        console.log('doc', doc);
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting files: ', error);
    });
};

export const uploadFile = (id, filename, callback) => {
  return storage
    .ref(folder)
    .child(filename)
    .getDownloadURL()
    .then(url => createFile(id, url, callback))
    .catch(function (error) {
      console.log('Error saving project: ', error);
    });
};

export const createFile = (projectId, url, callback) => {
  var file = {
    projectId,
    url,
  };
  return db
    .collection(collection)
    .add(file)
    .then(() => callback())
    .catch(function(error) {
      console.log('Error creating file: ', error);
    });
};

export const deleteFile = (id) => {
  return db
    .collection(collection)
    .doc(id)
    .delete()
    .catch(function(error) {
      console.log('Error deleting file: ', error);
    });
};
