import { db } from '../config/constants';
import { getCurrentUser } from '../helpers/auth';

const collection = 'matrix';
const collection_prob = 'probability_range';
const collection_impact = 'impact_range';

export const createMatrix = projectId => {
  var matrix = {
    projectId,
    userId: getCurrentUser().uid
  };
  return db
    .collection(collection)
    .add(matrix)
    .catch(function(error) {
      console.log('Error saving project: ', error);
    });
};

export const getProjectMatrix = projectId => {
  return db
    .collection(collection)
    .where('projectId', '==', projectId)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting documents: ', error);
    });
};

export const getProbabilityRange = () => {
  return db
    .collection(collection_prob)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting documents: ', error);
    });
};

export const getImpactRange = () => {
  return db
    .collection(collection_impact)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting documents: ', error);
    });
};
