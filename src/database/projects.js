import { db } from '../config/constants';
import { getCurrentUser } from '../helpers/auth';

const collection = 'projects';

export const getUserProjects = () => {
  return db
    .collection(collection)
    .where('userId', '==', getCurrentUser().uid)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting documents: ', error);
    });
};

export const createProject = (name, description) => {
  var project = {
    name,
    description,
    userId: getCurrentUser().uid
  };
  return db
    .collection(collection)
    .add(project)
    .catch(function(error) {
      console.log('Error saving project: ', error);
    });
};

export const updateProject = project => {
  var element = {
    name: project.name,
    description: project.description,
    userId: getCurrentUser().uid
  };
  return db
    .collection(collection)
    .doc(project.id)
    .update(element)
    .catch(function(error) {
      console.log('Error saving project: ', error);
    });
};

export const deleteProject = id => {
  return db
    .collection(collection)
    .doc(id)
    .delete()
    .then(() => {
      return true;
    })
    .catch(error => {
      console.log(`Error deleting project ${id}: `, error);
      return false;
    });
};

export const getProject = id => {
  return db
    .collection(collection)
    .doc(id)
    .get()
    .then(doc => {
      var item = doc.data();
      item.id = doc.id;
      return item;
    });
};
