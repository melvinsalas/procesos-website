
import { db } from '../config/constants';

const collection = 'resources';

export const getResources = (projectId) => {
  return db
    .collection(collection)
    .where('projectId', '==', projectId)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting resources: ', error);
    });
};

export const createResource = (projectId, name, value) => {
  var resource = {
    projectId,
    name,
    value,
  };
  return db
    .collection(collection)
    .add(resource)
    .catch(function (error) {
      console.log('Error saving resource: ', error);
    });
};

export const updateResource = (resource) => {
  return db
    .collection(collection)
    .doc(resource.id)
    .update(resource)
    .catch(function (error) {
      console.log('Error saving resource: ', error);
    });
};

export const deleteResource = id => {
  return db
    .collection(collection)
    .doc(id)
    .delete()
    .then(() => {
      return true;
    })
    .catch(error => {
      console.log(`Error deleting resource ${id}: `, error);
      return false;
    });
};
