import { db } from '../config/constants';
import { getCurrentUser } from '../helpers/auth';

const collection = 'risks';

export const createRisk = (
  matrixId,
  description,
  prob_value,
  impact_value,
  score,
  category
) => {
  var risk = {
    matrixId,
    description,
    probability: prob_value,
    impact: impact_value,
    score,
    category,
    userId: getCurrentUser().uid
  };
  return db
    .collection(collection)
    .add(risk)
    .catch(function(error) {
      console.log('Error saving project: ', error);
    });
};

export const getMatrixRisks = matrixId => {
  return db
    .collection(collection)
    .where('matrixId', '==', matrixId)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting documents: ', error);
    });
};

export const deleteRisk = id => {
  return db
    .collection(collection)
    .doc(id)
    .delete()
    .then(() => {
      return true;
    })
    .catch(error => {
      console.log(`Error deleting risk ${id}: `, error);
      return false;
    });
};
