import { db } from '../config/constants';

const collection = 'tasks';

export const getTasks = projectId => {
  return db
    .collection(collection)
    .where('projectId', '==', projectId)
    .get()
    .then(querySnapshot => {
      return querySnapshot.docs.map(doc => {
        var item = doc.data();
        item.id = doc.id;
        return item;
      });
    })
    .catch(function(error) {
      console.log('Error getting resources: ', error);
    });
};

export const createTask = (
  projectId,
  name,
  duration,
  porcentageCompleted,
  resourceId,
  workedHours,
  baseCost,
  actualCost
) => {
  var task = {
    projectId,
    name,
    duration,
    porcentageCompleted,
    resourceId,
    workedHours,
    baseCost,
    actualCost
  };
  return db
    .collection(collection)
    .add(task)
    .catch(function(error) {
      console.log('Error saving resource: ', error);
    });
};

export const updateTask = task => {
  return db
    .collection(collection)
    .doc(task.id)
    .update(task)
    .catch(function(error) {
      console.log('Error saving resource: ', error);
    });
};

export const deleteTask = id => {
  return db
    .collection(collection)
    .doc(id)
    .delete()
    .then(() => {
      return true;
    })
    .catch(error => {
      console.log(`Error deleting resource ${id}: `, error);
      return false;
    });
};
