export function getRiskCategory(score) {
  if (score <= 5) {
    return 'MUY BAJO';
  } else if (score <= 10) {
    return 'BAJO';
  } else if (score <= 15) {
    return 'MEDIO';
  } else if (score <= 20) {
    return 'ALTO';
  } else if (score <= 25) {
    return 'MUY ALTO';
  }
}

export function getCategoryColor(category) {
  switch (category) {
    case 'MUY BAJO':
      return { name: 'lime', hexa: '#7ED042' };
    case 'BAJO':
      return { name: 'yellow', hexa: '#F0C330' };
    case 'MEDIO':
      return { name: 'orange', hexa: '#FB964D' };
    case 'ALTO':
      return { name: 'pink', hexa: '#F46F9B' };
    case 'MUY ALTO':
      return { name: 'red', hexa: '#CB2327' };
    default:
      return { name: 'orange', hexa: '#FB964D' };
  }
}

export function getCategoryColorImpact(category) {
  switch (category) {
    case 'BAJO':
      return { name: 'lime', hexa: '#7ED042' };
    case 'MEDIO ALTO':
      return { name: 'yellow', hexa: '#F0C330' };
    case 'ALTO':
      return { name: 'orange', hexa: '#FB964D' };
    case 'MUY ALTO':
      return { name: 'pink', hexa: '#F46F9B' };
    case 'CRÍTICO':
      return { name: 'red', hexa: '#CB2327' };
    default:
      return { name: 'orange', hexa: '#FB964D' };
  }
}

export function getCategoryColorProb(category) {
  switch (category) {
    case 'MUY BAJO':
      return { name: 'lime', hexa: '#7ED042' };
    case 'BAJO':
      return { name: 'yellow', hexa: '#F0C330' };
    case 'MEDIA':
      return { name: 'orange', hexa: '#FB964D' };
    case 'FRECUENTE':
      return { name: 'pink', hexa: '#F46F9B' };
    case 'MUY FRECUENTE':
      return { name: 'red', hexa: '#CB2327' };
    default:
      return { name: 'orange', hexa: '#FB964D' };
  }
}

export function getDataForMatrix(risks) {
  const columns = [];
  const colors = {};
  const totalRisks = risks.length;
  const types = [
    { name: 'MUY BAJO', quantity: 0 },
    { name: 'BAJO', quantity: 0 },
    { name: 'MEDIO', quantity: 0 },
    { name: 'ALTO', quantity: 0 },
    { name: 'MUY ALTO', quantity: 0 }
  ];
  risks.forEach(risk => {
    types.forEach(type => {
      if (risk.category === type.name) {
        type.quantity++;
      }
    });
  });

  types.forEach(type => {
    if (type.quantity > 0) {
      columns.push([type.name, getPorcentage(type.quantity, totalRisks)]);
      colors[type.name] = getCategoryColor(type.name).hexa;
    }
  });
  return { columns, colors };
}

function getPorcentage(quantity, total) {
  return (quantity * 100) / total;
}
